import Constants from 'expo-constants';

type Channel = undefined | 'published' | 'production';

const Channel = Constants.manifest.releaseChannel as Channel;

const getConfig = () => {
  if (Channel === 'published') {
    return {
      apiUrl: 'API_URL_FROM_AWS'
    };
  }
  if (Channel === 'production') {
    return ({} as unknown) as config.IConfig;
  }
  return {
    apiUrl: 'http://ec2-3-10-58-174.eu-west-2.compute.amazonaws.com:4000'
  };
};

const config: config.IConfig = {
  ...getConfig()
};

export default config;
