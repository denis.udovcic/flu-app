import { StyleSheet } from 'react-native';

const { boxShadow } = StyleSheet.create({
  boxShadow: {
    elevation: 3,
    shadowOffset: { height: 5, width: 3 },
    shadowRadius: 4,
    shadowOpacity: 0.95
  }
});

export default {
  colors: {
    green: '#00FA9A'
  },
  boxShadow
};
