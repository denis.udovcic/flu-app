import { HttpClient } from './http';
import { AxiosError, AxiosResponse } from 'axios';

export default class Api extends HttpClient {
  constructor(protected config: config.IConfig) {
    super({
      baseURL: config.apiUrl,
      onError: (apiError: AxiosError) => {
        throw apiError;
      },
      responseDataMapper(res: AxiosResponse<any>) {
        return res.data;
      }
    });
  }

  public checkFlu = (
    data: api.models.ITestRequest
  ): Promise<api.models.ITestResponse> =>
    this.post<api.models.ITestResponse>('/tests/flu', data);
}
