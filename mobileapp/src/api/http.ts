import Axios, {
  AxiosInstance,
  AxiosError,
  AxiosResponse,
  AxiosRequestConfig
} from 'axios';

export interface HTTPClientOptions {
  baseURL: string;
  onError(error: AxiosError): void;
  responseDataMapper<T>(res: AxiosResponse<any>): T;
}

export class HttpClient {
  private axiosClient: AxiosInstance;

  constructor(protected options: HTTPClientOptions) {
    this.axiosClient = Axios.create({
      baseURL: options.baseURL,
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
  protected post = <T>(
    url: string,
    data?: object,
    axiosReqConfig?: AxiosRequestConfig
  ): Promise<T> =>
    this.axiosClient
      .post(url, data, axiosReqConfig)
      .then((res: AxiosResponse) => this.options.responseDataMapper<T>(res))
      .catch(this.options.onError) as Promise<T>;
}
