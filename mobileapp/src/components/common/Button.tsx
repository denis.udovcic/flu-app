import React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  ActivityIndicator
} from 'react-native';
import variables from '../../styles/variables';

interface Props {
  loading?: boolean;
  text: string;
  disabled?: boolean;
  onPress(): void;
}

export default (props: Props) => {
  return (
    <TouchableOpacity
      style={styles.wrapper}
      onPress={props.disabled ? () => void 0 : props.onPress}
    >
      <View style={[styles.container, { opacity: props.disabled ? 0.4 : 1 }]}>
        {props.loading ? (
          <ActivityIndicator style={styles.loadingSpinner} />
        ) : (
          <Text>{props.text}</Text>
        )}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    width: '100%'
  },
  container: {
    width: '100%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: variables.colors.green,
    ...variables.boxShadow
  },
  loadingSpinner: {
    width: 15,
    height: 15
  },
  text: {
    fontSize: 12,
    fontWeight: 'bold'
  }
});
