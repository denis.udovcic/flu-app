import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  TouchableOpacity
} from 'react-native';
import Api from './api';
import DropdownAlert from 'react-native-dropdownalert';
import config from './config';
import Button from './components/common/Button';
import variables from './styles/variables';

const api = new Api(config);

interface State {
  answer?: boolean;
  cough?: boolean;
  fluLast5?: boolean;
  temperature?: number;
  loading: boolean;
}

export default class App extends React.Component<never, State> {
  dropDownAlertRef?: DropdownAlert;
  state: State = {
    answer: undefined,
    cough: undefined,
    fluLast5: undefined,
    temperature: undefined,
    loading: false
  };
  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style={styles.container}>
          <View style={{ alignItems: 'center' }}>
            <Text>Any cough ?</Text>
            <this.Box
              text="Yes"
              selected={this.state.cough}
              onClick={() => this.setState({ cough: true })}
            />
            <this.Box
              text="No"
              selected={this.state.cough === false}
              onClick={() => this.setState({ cough: false })}
            />
          </View>
          <View style={{ alignItems: 'center' }}>
            <Text>Flu within last 5 days ?</Text>
            <this.Box
              text="Yes"
              selected={this.state.fluLast5}
              onClick={() => this.setState({ fluLast5: true })}
            />
            <this.Box
              text="No"
              selected={this.state.fluLast5 === false}
              onClick={() => this.setState({ fluLast5: false })}
            />
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginVertical: 20
            }}
          >
            <Text>Temperature ?</Text>
            <TextInput
              style={{
                width: 100,
                height: 50,
                borderRadius: 15,
                borderWidth: 0.3,
                borderColor: 'black'
              }}
              keyboardType="number-pad"
              value={`${this.state.temperature || ''}`}
              onChangeText={t => this.setState({ temperature: Number(t) })}
            />
          </View>
          <View style={{ width: 200 }}>
            <Button
              disabled={this.validate()}
              loading={this.state.loading}
              text="Checl"
              onPress={this.onCheck}
            />
          </View>
          <DropdownAlert ref={ref => (this.dropDownAlertRef = ref!)} />
        </View>
      </TouchableWithoutFeedback>
    );
  }

  private Box = (props: {
    text: string;
    onClick(): void;
    selected?: boolean;
  }) => {
    return (
      <TouchableOpacity
        onPress={props.onClick}
        style={[
          styles.box,
          typeof props.selected === 'boolean' && {
            backgroundColor: props.selected ? variables.colors.green : 'white'
          }
        ]}
      >
        <Text>{props.text}</Text>
      </TouchableOpacity>
    );
  };

  private onCheck = async () => {
    this.setState({ loading: true });
    const { cough, fluLast5, temperature } = this.state;
    try {
      if (
        cough !== undefined &&
        fluLast5 !== undefined &&
        temperature !== undefined
      ) {
        const { flu: answer } = await api.checkFlu({
          cough,
          fluLast5,
          temperature
        });
        if (this.dropDownAlertRef) {
          this.dropDownAlertRef.alertWithType(
            answer ? 'error' : 'success',
            'Flu check result',
            answer ? 'You got it' : 'Lucky you'
          );
        }
      }
    } finally {
      this.setState({ loading: false });
    }
  };

  private validate = () =>
    [this.state.cough, this.state.fluLast5, this.state.temperature].some(
      v => v === undefined
    );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    width: 150,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    borderWidth: 0.3,
    borderColor: 'black'
  },
  box: {
    width: 80,
    height: 60,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.3,
    borderColor: 'black'
  }
});
