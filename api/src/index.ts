import { createApp } from './app';
import { startServer } from './server';
import config from './config';

(async (): Promise<void> => {
  const app = await createApp(config);

  await startServer(app, config);
})();
