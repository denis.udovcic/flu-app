import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import routes from './routes';
import { reqLogger } from './middleware/request-logger';

export const createApp = async (
  config: config.IConfig
): Promise<express.Express> => {
  const app: express.Express = express();

  app.set('port', config.port);
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json({ limit: 1024 * 1024 * 5 }));
  app.use(cors());
  app.use(reqLogger);
  routes(app, config);

  return app;
};
