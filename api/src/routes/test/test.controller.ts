import { UnauthTypedRequest } from '../../middleware/request-middleware';
import { badRequest } from '../../errors/error-response';
import { validateFluTestData, hasFlu } from '../utils/test-utils';

export default class TestController {
  constructor(protected config: config.IConfig) {}

  public fluTest = async (
    req: UnauthTypedRequest<void, api.models.ITestRequest, void>
  ): IApiResponse<api.models.ITestResponse> => {
    const { body: data } = req;

    const validationErrors = validateFluTestData(data);

    if (validationErrors.length) {
      return badRequest('Validation Error!', validationErrors);
    }

    const flu = hasFlu(data);

    return {
      flu
    };
  };
}
