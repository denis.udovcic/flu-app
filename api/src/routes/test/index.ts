import { Router } from 'express';
import TestController from './test.controller';
import { UnauthorisedRouteHandlerBuilder } from '../utils/route-utils';

const testController = (c: config.IConfig): TestController =>
  new TestController(c);

const testsRouter = Router();

export const routes = (
  unauthRequest: UnauthorisedRouteHandlerBuilder
): Router => {
  testsRouter.post(
    '/flu',
    unauthRequest<
      void,
      api.models.ITestRequest,
      void,
      api.models.ITestResponse,
      TestController
    >(testController, (c: TestController) => c.fluTest)
  );
  return testsRouter;
};
