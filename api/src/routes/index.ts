import { Application } from 'express';
import * as tests from './test';
import { unauthRequestHandlerBuilder } from './utils/route-utils';

const routesConfig = (app: Application, config: config.IConfig): void => {
  const unauthRequest = unauthRequestHandlerBuilder(config);
  app.use('/tests', tests.routes(unauthRequest));
};

export default routesConfig;
