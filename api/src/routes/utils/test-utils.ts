export const validateFluTestData = (
  data?: api.models.ITestRequest
): string[] => {
  const errors: string[] = [];
  if (!data) {
    errors.push('Test data not found!');
    return errors;
  }
  const { cough, fluLast5, temperature } = data;

  if (
    typeof cough !== 'boolean' ||
    typeof fluLast5 !== 'boolean' ||
    typeof temperature !== 'number'
  ) {
    errors.push('Data not in valid format!');
  }

  return errors;
};

export const hasFlu: (data: api.models.ITestRequest) => boolean = ({
  cough,
  fluLast5,
  temperature
}) => cough && fluLast5 && temperature > 38;
