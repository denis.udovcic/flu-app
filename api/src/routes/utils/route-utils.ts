import { RequestHandler } from 'express';
import {
  TypedRequestHandler,
  unauthRequest,
  UnauthTypedRequest
} from '../../middleware/request-middleware';

export type UnauthorisedRouteHandlerBuilder = <P, B, Q, Res, C>(
  controller: (c: config.IConfig) => C,
  action: (
    c: C
  ) => TypedRequestHandler<UnauthTypedRequest<P, B, Q>, IApiResponse<Res>>
) => RequestHandler;

export const unauthRequestHandlerBuilder = (
  c: config.IConfig
): UnauthorisedRouteHandlerBuilder => {
  return <C, P, B, Q, Res>(
    controller: (c: config.IConfig) => C,
    action: (
      c: C
    ) => TypedRequestHandler<UnauthTypedRequest<P, B, Q>, IApiResponse<Res>>
  ): RequestHandler => unauthRequest(c, controller, action);
};
