import * as moment from 'moment';

export const dateToUTCEpoch = (time: any): number => {
  return moment.utc(time).valueOf();
};
