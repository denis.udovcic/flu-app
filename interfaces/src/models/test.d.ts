declare module api.models {
  interface ITestRequest {
    cough: boolean;
    fluLast5: boolean;
    temperature: number;
  }
  interface ITestResponse {
    flu: boolean;
  }
}
